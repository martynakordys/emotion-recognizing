import tensorflow as tf

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import os
import numpy as np
import matplotlib.pyplot as plt
import sys
import distutils
import distutils.util

def plotImages(images_arr):
    fig, axes = plt.subplots(1, 5, figsize=(imgHeight,imgWidth))
    axes = axes.flatten()
    for img, ax in zip( images_arr, axes):
        ax.imshow(img)
        ax.axis('off')
    plt.tight_layout()
    plt.show()
    
#sciezka do folderu z danymi
path = sys.argv[1]
horizontalFlip = bool(distutils.util.strtobool(sys.argv[2]))
rotationRange = float(sys.argv[3])
zoomRange = float(sys.argv[4])
#path = "D:\\biaiproject\\pythonCNN\\dataset\\face-expression-recognition-dataset\\images"

trainDir = os.path.join(path, 'train') # directory with training pictures
validateDir = os.path.join(path, 'validation') # directory with validation pictures

trainAngerDir = os.path.join(trainDir, 'angry')  # directory with training anger pictures
trainHappyDir = os.path.join(trainDir, 'happy')  # directory with training happy pictures
trainNeutralDir = os.path.join(trainDir, 'neutral')
trainSurpriseDir = os.path.join(trainDir, 'surprise')
trainSadDir = os.path.join(trainDir, 'sad')
validationAngerDir = os.path.join(validateDir, 'angry')  # directory with validation anger pictures
validationHappyDir = os.path.join(validateDir, 'happy')  # directory with validation happy pictures
validationNeutralDir = os.path.join(validateDir, 'neutral')
validationSurpriseDir = os.path.join(validateDir, 'surprise')
validationSadDir = os.path.join(validateDir, 'sad')

#zliczenie liczby obrazkow w folderach
numAngerTr = len(os.listdir(trainAngerDir))
numHappyTr = len(os.listdir(trainHappyDir))
numNeutralTr = len(os.listdir(trainNeutralDir))
numSurpriseTr = len(os.listdir(trainSurpriseDir))
#numSadTr = len(os.listdir(trainSadDir))
numAngerVal = len(os.listdir(validationAngerDir))
numHappyVal = len(os.listdir(validationHappyDir))
numNeutralVal = len(os.listdir(validationNeutralDir))
numSurpriseVal = len(os.listdir(validationSurpriseDir))
#numSadVal = len(os.listdir(validationSadDir))

totalTrain = numAngerTr + numHappyTr + numNeutralTr + numSurpriseTr #+ numSadTr
totalValid = numAngerVal + numHappyVal + numNeutralVal + numSurpriseVal #+ numSadVal

print('total training anger images:', numAngerTr)
print('total training happy images:', numHappyTr)
print('total training neutral images:', numNeutralTr)
print('total training suprise images:', numSurpriseTr)
#print('total training sad images:', numSadTr)

print('total validation anger images:', numAngerVal)
print('total validation happy images:', numHappyVal)
print('total validation neutral images:', numNeutralVal)
print('total validation suprise images:', numSurpriseVal)
#print('total validation sad images:', numSadVal)
print("  ")
print("Total training images:", totalTrain)
print("Total validation images:", totalValid)

batchSize = 128
epochs = 21
imgHeight = 48
imgWidth = 48

trainImageGenerator = ImageDataGenerator(rescale = 1. / 255, rotation_range = rotationRange, horizontal_flip = horizontalFlip, zoom_range = zoomRange) # Generator for our training data
validationImageGenerator = ImageDataGenerator(rescale = 1. / 255) # Generator for our validation data

trainDataGen = trainImageGenerator.flow_from_directory(batch_size = batchSize, directory = trainDir, shuffle = True, target_size = (imgHeight, imgWidth), classes=['angry', 'happy', 'neutral', 'surprise'], class_mode = 'categorical')
valDataGen = validationImageGenerator.flow_from_directory(batch_size = batchSize, directory = validateDir, shuffle = True, target_size = (imgHeight, imgWidth), classes=['angry', 'happy', 'neutral', 'surprise'], class_mode = 'categorical')
augmented_images = [trainDataGen[0][0][0] for i in range(5)]
plotImages(augmented_images)
sample_training_images, _ = next(trainDataGen)
plotImages(sample_training_images[:5])

model = Sequential([
    Conv2D(16, 3, padding='same', activation='relu', input_shape=(imgHeight, imgWidth, 3)),
    MaxPooling2D(),
    Conv2D(32, 3, padding='same', activation='relu'),
    MaxPooling2D(),
    Conv2D(64, 3, padding='same', activation='relu'),
    MaxPooling2D(),
    Flatten(),
    Dense(512, activation='relu'),
    Dense(4, activation='softmax')
])

model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=['accuracy'])

#ustawienie learning rate
#model.optimizer.lr = 0.001
model.summary()

history = model.fit(
    trainDataGen,
    steps_per_epoch = totalTrain // batchSize,
    epochs = epochs,
    validation_data = valDataGen,
    validation_steps = totalValid // batchSize
)

acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss=history.history['loss']
val_loss=history.history['val_loss']

epochs_range = range(epochs)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.title('Training and Validation Accuracy')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.title('Training and Validation Loss')
plt.show()

model.save("CNNmodel5.h5")
