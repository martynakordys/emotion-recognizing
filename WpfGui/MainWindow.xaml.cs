﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace BiaiProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly MainPage mainPage;
        private readonly TrainingPage trainingPage;
        private readonly InformationPage informationPage;

        public MainWindow()
        {
            InitializeComponent();
            mainPage = new MainPage();
            trainingPage = new TrainingPage();
            informationPage = new InformationPage();
            mainGrid.Children.Add(mainPage);
        }
        private void CloseButton_Click(object sender, RoutedEventArgs e) => Application.Current.Shutdown();
        private void MinimalizeButton_Click(object sender, RoutedEventArgs e) => WindowState = WindowState.Minimized;
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e) => DragMove();
        private void MenuListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = MenuListView.SelectedIndex;
            MoveCursorMenu(index);

            switch (index)
            {
                case 0:
                    mainGrid.Children.Clear();
                    mainGrid.Children.Add(mainPage);
                    break;
                case 1:
                    mainGrid.Children.Clear();
                    mainGrid.Children.Add(trainingPage);
                    break;
                case 2:
                    mainGrid.Children.Clear();
                    mainGrid.Children.Add(informationPage);
                    break;
                default:
                    mainGrid.Children.Clear();
                    break;
            }
        }

        private void MoveCursorMenu(int index)
        {
            TransitioningContentSlide.OnApplyTemplate();
            cursorGrid.Margin = new Thickness(0, cursorGrid.ActualHeight * index + 60 + (index * 20), 0, 0);
        }
    }
}
